<%@page import="core.ResponseMessageDataTransfer"%>
<%@page import="core.RequestMessageDataTransfer"%>
<%@page import="core.DataClass"%>
<%@page import="java.lang.reflect.*"%>
<%@page  import= "java.util.LinkedHashMap"%>
<%@page  import= "java.util.ArrayList"%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" %>
    
 <jsp:include page="/Servlet" flush="false">
<jsp:param name="action" value="getSessionKeyFromJSP" />
</jsp:include>

<jsp:include page="/Servlet" flush="false">
	<jsp:param name="action" value="getVbsAddress" />
</jsp:include>
    
<%   

String manuName = (String)session.getAttribute("user");

%>
      
<!DOCTYPE html>
<html>
<head>

<!--   curl -i -H "Content-Type:application/json" -H "Accept: application/json" -X POST -d '{"protocol":"1.4","id":"null","session":"null","type":"Ping","data":"null","seq":1}' -v http://204.68.122.215:8080/app-proc -->


<link rel="stylesheet" type="text/css" href="styles/CarSyncStyles.css"/>

<link rel="stylesheet" href="styles/jquery-ui.css" />

<script type="text/javascript" src="javascript/jquery/jquery-1.10.2.min.js"> </script>
  
<script type="text/javascript" src="javascript/jquery-ui-1.10.3/ui/jquery-ui.js"> </script>

<script type="text/javascript" src="javascript/CarSyncJS.js"></script>

 <script type="text/javascript">

</script>


<script type="text/javascript">

	getSessionKey();
	getBasicData();

</script>	
 
  
<title> Super User Administration Panel </title>

</head>

<body>

    <div id = "page">
    
    <div id = "containerMain">
    
    <!-- Hidden divs -->
    
        <div id="feedBackTab">

		&nbsp;<img src="img/close-button.png" id="feedTabCloseButton"
			class="closeButtons" onclick="$('#feedBackTab').hide(100)"><br>
		<img src="img/success.png" id="successExc">

		<div id="feedTabInnerDiv"></div>

	</div>
	
		<div id="errorTab">

		&nbsp;<img src="img/close-button.png" id="errorTabCloseButton"
			class="closeButtons" onclick="$('#errorTab').hide()"><br>
		<img src="img/errorExc.png" id="errorExc">

		<div id="errorTabInnerDiv"></div>

	</div>
    
   
    <div id="addManuDiv">

                  <img src="img/close-button.png" id="errorTabCloseButton"
			class="closeButtons" onclick="$('#addManuDiv').hide()">

		
			<table id="addMAnuTable">

         
					<tr>
						<td class = "colouredClass">
							<p id=""  >Name :</p>
						</td>
						<td ><input type="text" id="manuName" class="addInput" /></td>
					</tr>

					<tr>
						<td class = "colouredClass">
							<p id=""  class = "colouredClass">Description :</p>
						</td>
						<td ><input type="text" id="manuDesc" class="addInput" /></td>
					</tr>
					<tr>
						<td class = "colouredClass">
							<p id=""  >Country :</p> </td>
						<td><input type="text" id="manuCountry" class="addInput" /></td>
					</tr>
					<tr>
						<td class = "colouredClass">
							<p id=""  >www :</p></td>
						<td><input type="text" id="manuWWW" class="addInput" /></td>
					</tr>
					
					<tr>
						<td class = "colouredClass">
							<p id=""  >mail :</p></td>
						<td><input type="text" id="manuMail" class="addInput" /></td>
					</tr>
		         </table>
		         
		         <br>
		         
		         <button id = "addManuBtn" onclick = "createManu()">
		         Add Manufacturer
		         </button>


	  </div>
	
       <%@include file="headerSuper.txt"  %>
    
      <div id = "adminTasks">
    
       <input type = "text" id = "setVBSip"  class = "superClass" >
       
       <button   id = "setVBSipButton"  onclick = "validationFunction('setVBSip') ; setVBSip()">
       Set the VBS address
       </button>
       <br>
       
      
       
       
       <input type = "text" id = "setKey"   class = "superClass"  >
       
       <button   id = "setSessionKey"  onclick = "validationFunction('setKey') ; setSessionKey()">
       Set session key
       </button>
       <br>
       
       <hr id = "hrAdminTasks">
       
       <br>
       
       <img src = "img/5.add_new_user.png"  alt = "Add Manufacturer" onclick = "openAddManuDiv()" />   
           
       </div>
       
       
        <p> Current VBS address :</p> &nbsp;<%=(String)session.getAttribute("vbsAddress")%>
        
           <p> Current session key :</p> &nbsp;<%=(String)session.getAttribute("sessionKey")%>
    
    </div> 
    
    </div>
 
 
</body>



</html>