 package model;
 
 import core.ReturnMultipleValues;

import java.sql.Connection;
/*   4:    *//*   5:    */ /*   6:    */ import java.sql.DriverManager;
/*   7:    */ import java.sql.PreparedStatement;
/*   8:    */ import java.sql.ResultSet;
/*   9:    */ import java.sql.SQLException;
/*  10:    */ import java.sql.Statement;
/*  11:    */ import java.util.HashMap;
/*  12:    */ import java.util.Map;

/*  13:    */ import javax.servlet.http.HttpServletRequest;
/*  14:    */ import javax.servlet.http.HttpSession;
/*  15:    */ 
/*  16:    */ public class DbBean
/*  17:    */ {
/*  18: 21 */   private static final DbBean dbInstance = new DbBean();
/*  19:    */   
/*  20:    */   public static DbBean getInstance()
/*  21:    */   {
/*  22: 25 */     return dbInstance;
/*  23:    */   }
/*  24:    */   
/*  25: 28 */   String myDataField = null;
/*  26: 29 */   Connection myConnection = null;
/*  27: 30 */   PreparedStatement myPreparedStatement = null;
   String test = "test";
   
   private DbBean()
  {
    try
     {
       String driver = "org.postgresql.Driver";
        
      String url = "jdbc:postgresql://localhost:5432/carSyncBackofficeDb";   //  to zawsze wskaze na odpowiednia baze  - DEPLOY WAR !!!
       //String url = "jdbc:postgresql://arynga.pl:50232/carSyncBackofficeDb";  //  production database
       //String url = "jdbc:postgresql://arynga.pl:51232/carSyncBackofficeDb";   // test database  
     //  String url = "jdbc:postgresql://arynga.pl:52032/carSyncBackofficeDb";  //  dev database
       
      
      String username = "admin";
      String password = "qwerty12";
      try
     {
        Class.forName(driver).newInstance();
      }
     catch (InstantiationException e)
      {
        e.printStackTrace();
      }
      catch (IllegalAccessException e)
       {
       e.printStackTrace();
       }
      try
     {
        this.myConnection = DriverManager.getConnection(url, username, password);
      }
      catch (SQLException e)
/*  59:    */       {
/*  60: 60 */         e.printStackTrace();
/*  61:    */       }
/*  62: 71 */       return;
/*  63:    */     }
/*  64:    */     catch (ClassNotFoundException e)
/*  65:    */     {
/*  66: 67 */       e.printStackTrace();
/*  67:    */     }
/*  68:    */   }
/*  69:    */   
/*  70:    */   public ReturnMultipleValues verifyCredentials(String inputName, String inputPass, HttpServletRequest request)
/*  71:    */   {
/*  72: 81 */     System.out.println("Verify creds was ran");
/*  73:    */     
/*  74: 83 */     Statement stmt = null;
/*  75:    */     try
/*  76:    */     {
/*  77: 86 */       stmt = this.myConnection.createStatement();
/*  78:    */     }
/*  79:    */     catch (SQLException e)
/*  80:    */     {
/*  81: 89 */       System.out.println("Database is not started");
/*  82: 90 */       e.printStackTrace();
/*  83:    */     }
/*  84:    */     try
/*  85:    */     {
/*  86: 95 */       ResultSet rs = stmt.executeQuery("SELECT * FROM users");
/*  87: 99 */       while (rs.next())
/*  88:    */       {
/*  89:101 */         String dbName = rs.getString("user_login");
/*  90:102 */         String dbPass = rs.getString("user_password");
                     

                       /*
                        *    Here it checks if the user is a superuser . 
                        * 
                        */


       boolean isSuperUser = rs.getBoolean(4);
      //  boolean isSuperUser = rs.getBoolean("\"isSuperUser\"");
        if (dbName.equals(inputName))
        {
          System.out.println("name passed");
          if (dbPass.equals(inputPass))
         {
            ReturnMultipleValues retur = new ReturnMultipleValues();
            
           retur.setPassed(true);
            retur.setIssuper(isSuperUser);
            
            System.out.println("password ok");
            
            HttpSession session = request.getSession(true);
             session.setAttribute("user", dbName);
            
            return retur;
          }
          return new ReturnMultipleValues();
         }
      }
    }
    catch (SQLException e)
     {
      e.printStackTrace();
    }

    return new ReturnMultipleValues();
  }
  
   public void setKey(String key)
  {
    Statement stmt = null;
    try
    {
       stmt = this.myConnection.createStatement();
     }
     catch (SQLException e)
     {
      System.out.println("Database is not started");
      e.printStackTrace();
     }
    try
     {
     int e = stmt.executeUpdate("INSERT INTO system_settings (key) VALUES ('" + key + "');");
    }
    catch (SQLException e)
     {
      e.printStackTrace();
     }
  }
  
   public void setAddrrInDb(String address)
  {
    Statement stmt = null;
    try
    {
     stmt = this.myConnection.createStatement();
/* 148:    */     }
/* 149:    */     catch (SQLException e)
/* 150:    */     {
/* 151:181 */       System.out.println("Database is not started");
/* 152:182 */       e.printStackTrace();
/* 153:    */     }
/* 154:    */     try
/* 155:    */     {
/* 156:187 */      int e = stmt.executeUpdate("INSERT INTO system_settings (ipaddress) VALUES ('" + address + "');");
/* 157:    */     }
/* 158:    */     catch (SQLException e)
/* 159:    */     {
/* 160:193 */       e.printStackTrace();
/* 161:    */     }
/* 162:    */   }
/* 163:    */   
/* 164:    */   public Map<String, String> printWidgetState()
/* 165:    */   {
/* 166:202 */     Map<String, String> m = new HashMap();
/* 167:203 */     System.out.println("Verify creds was ran");
/* 168:    */     
/* 169:205 */     Statement stmt = null;
/* 170:    */     try
/* 171:    */     {
/* 172:208 */       stmt = this.myConnection.createStatement();
                 }
                       catch (SQLException e)
                 {
                     System.out.println("Database is not started");
                    e.printStackTrace();
                 }
                     try
                   {
                        ResultSet rs = stmt.executeQuery("SELECT widget_id , widget_name , active FROM widgets ");
                  while (rs.next())
/* 183:    */       {
/* 184:223 */         String widName = rs.getString("widget_name");
/* 185:    */         
/* 186:225 */         String active = rs.getString("active");
/* 187:    */         
/* 188:227 */         m.put(widName, active);
/* 189:    */       }
/* 190:    */     }
/* 191:    */     catch (SQLException e)
/* 192:    */     {
/* 193:234 */       e.printStackTrace();
/* 194:    */     }
/* 195:236 */     return m;
/* 196:    */   }
/* 197:    */   
/* 198:    */   public void deactivateWidget(String wname)
/* 199:    */   {
/* 200:248 */     Map<String, String> m = new HashMap();
/* 201:    */     
/* 202:250 */     Statement stmt = null;
/* 203:    */     try
/* 204:    */     {
/* 205:253 */       stmt = this.myConnection.createStatement();
/* 206:    */     }
/* 207:    */     catch (SQLException e)
/* 208:    */     {
/* 209:256 */       System.out.println("Database is not started");
/* 210:257 */       e.printStackTrace();
/* 211:    */     }
/* 212:    */     try
/* 213:    */     {
/* 214:261 */       System.out.println("deac widget" + wname);
/* 215:262 */     int  e = stmt.executeUpdate("UPDATE widgets SET active = 'false' WHERE widget_name ='" + wname + "'");
/* 216:    */     }
/* 217:    */     catch (SQLException e)
/* 218:    */     {
/* 219:266 */       e.printStackTrace();
/* 220:    */     }
/* 221:    */   }
/* 222:    */   
/* 223:    */   public String getManuId(String name)
/* 224:    */   {
/* 225:277 */     Statement stmt = null;
/* 226:278 */     String manuid = "";
/* 227:    */     try
/* 228:    */     {
/* 229:281 */       stmt = this.myConnection.createStatement();
/* 230:    */     }
/* 231:    */     catch (SQLException e)
/* 232:    */     {
/* 233:284 */       System.out.println("Database is not started");
/* 234:285 */       e.printStackTrace();
/* 235:    */     }
/* 236:    */     try
/* 237:    */     {
/* 238:290 */       ResultSet rs = stmt.executeQuery("SELECT manu_id FROM users WHERE user_login = '" + name + "'");
/* 239:292 */       while (rs.next()) {
/* 240:294 */         if ((rs.getString("manu_id") != null) && (rs.getString("manu_id") != "")) {
/* 241:296 */           manuid = rs.getString("manu_id");
/* 242:    */         }
/* 243:    */       }
/* 244:    */     }
/* 245:    */     catch (SQLException e)
/* 246:    */     {
/* 247:304 */       e.printStackTrace();
/* 248:    */     }
/* 249:307 */     return manuid;
/* 250:    */   }
/* 251:    */   
/* 252:    */   public void activateWidget(String wname)
/* 253:    */   {
/* 254:322 */     Map<String, String> m = new HashMap();
/* 255:    */     
/* 256:324 */     Statement stmt = null;
/* 257:    */     try
/* 258:    */     {
/* 259:327 */       stmt = this.myConnection.createStatement();
/* 260:    */     }
/* 261:    */     catch (SQLException e)
/* 262:    */     {
/* 263:330 */       System.out.println("Database is not started");
/* 264:331 */       e.printStackTrace();
/* 265:    */     }
/* 266:    */     try
/* 267:    */     {
/* 268:336 */     int  e = stmt.executeUpdate("UPDATE widgets SET active = 'true' WHERE widget_name ='" + wname + "';");
/* 269:    */     }
/* 270:    */     catch (SQLException e)

    {
     e.printStackTrace();
     }
  }
 
   public String getKey()
  {
    Statement stmt = null;
     String key = "";
     try
     {
    stmt = this.myConnection.createStatement();
/* 283:    */     }
/* 284:    */     catch (SQLException e)
/* 285:    */     {
/* 286:356 */       System.out.println("Database is not started");
/* 287:357 */       e.printStackTrace();
/* 288:    */     }
/* 289:    */     try
/* 290:    */     {
/* 291:362 */       ResultSet rs = stmt.executeQuery("SELECT key FROM system_settings");
/* 292:364 */       while (rs.next()) {
/* 293:366 */         if ((rs.getString("key") != null) && (rs.getString("key") != "")) {
/* 294:368 */           key = rs.getString("key");
/* 295:    */         }
/* 296:    */       }
/* 297:    */     }
/* 298:    */     catch (SQLException e)
/* 299:    */     {
/* 300:379 */       e.printStackTrace();
/* 301:    */     }
/* 302:384 */     return key;
/* 303:    */   }
/* 304:    */   
/* 305:    */   public String getIpAddrr()
/* 306:    */   {
/* 307:393 */     Statement stmt = null;
/* 308:394 */     String ad = "";
/* 309:    */     try
/* 310:    */     {
/* 311:397 */       stmt = this.myConnection.createStatement();
/* 312:    */     }
/* 313:    */     catch (SQLException e)
/* 314:    */     {
/* 315:400 */       System.out.println("Database is not started");
/* 316:401 */       e.printStackTrace();
/* 317:    */     }
/* 318:    */     try
     {
      ResultSet rs = stmt.executeQuery("SELECT ipaddress FROM system_settings");
       while (rs.next())
      {
         if (rs.getString("ipaddress") != null) {
           ad = rs.getString("ipaddress");
        }
        System.out.println("address from db" + ad);
       }
     }
     catch (SQLException e)
     {
      e.printStackTrace();
    }
     return ad;
  }




  public void createManu(String login, String pass , String manuid ){
	  
	  
	    Statement stmt = null;
	    try
	    {
	       stmt = this.myConnection.createStatement();
	     }
	     catch (SQLException e)
	     {
	      System.out.println("Database is not started");
	      e.printStackTrace();
	     }
	    try
	     {
	     int e = stmt.executeUpdate("INSERT INTO users (user_login,user_password,manu_id , \"isSuperUser\") VALUES ('" + login + "','" + pass + "','" + manuid + "','false');");
	    }
	    catch (SQLException e)
	     {
	      e.printStackTrace();
	     }
	    
	  
	  
  }



   





 }








