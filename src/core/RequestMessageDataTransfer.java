/*  1:   */ package core;
/*  2:   */ 
/*  3:   */ public class RequestMessageDataTransfer
/*  4:   */ {
/*  5: 5 */   private String protocol = null;
/*  6: 6 */   private String id = null;
/*  7: 7 */   private String session = null;
/*  8: 8 */   private String type = null;
/*  9: 9 */   private Object data = null;
/* 10:10 */   private String seq = null;
/* 11:   */   
/* 12:   */   public RequestMessageDataTransfer(String protocol, String id, String session, String type, Object data, String seq)
/* 13:   */   {
/* 14:15 */     this.protocol = protocol;
/* 15:16 */     this.id = id;
/* 16:17 */     this.session = session;
/* 17:18 */     this.type = type;
/* 18:19 */     this.data = data;
/* 19:20 */     this.seq = seq;
/* 20:   */   }
/* 21:   */   
/* 22:   */   public String getProtocol()
/* 23:   */   {
/* 24:25 */     return this.protocol;
/* 25:   */   }
/* 26:   */   
/* 27:   */   public void setProtocol(String protocol)
/* 28:   */   {
/* 29:28 */     this.protocol = protocol;
/* 30:   */   }
/* 31:   */   
/* 32:   */   public String getId()
/* 33:   */   {
/* 34:31 */     return this.id;
/* 35:   */   }
/* 36:   */   
/* 37:   */   public void setId(String id)
/* 38:   */   {
/* 39:35 */     this.id = id;
/* 40:   */   }
/* 41:   */   
/* 42:   */   public String getSession()
/* 43:   */   {
/* 44:38 */     return this.session;
/* 45:   */   }
/* 46:   */   
/* 47:   */   public void setSession(String session)
/* 48:   */   {
/* 49:41 */     this.session = session;
/* 50:   */   }
/* 51:   */   
/* 52:   */   public String getType()
/* 53:   */   {
/* 54:44 */     return this.type;
/* 55:   */   }
/* 56:   */   
/* 57:   */   public void setType(String type)
/* 58:   */   {
/* 59:47 */     this.type = type;
/* 60:   */   }
/* 61:   */   
/* 62:   */   public Object getData()
/* 63:   */   {
/* 64:50 */     return this.data;
/* 65:   */   }
/* 66:   */   
/* 67:   */   public void setData(String data)
/* 68:   */   {
/* 69:53 */     this.data = data;
/* 70:   */   }
/* 71:   */   
/* 72:   */   public String getSeq()
/* 73:   */   {
/* 74:56 */     return this.seq;
/* 75:   */   }
/* 76:   */   
/* 77:   */   public void setSeq(String seq)
/* 78:   */   {
/* 79:59 */     this.seq = seq;
/* 80:   */   }
/* 81:   */ }


/* Location:           C:\Users\T430\Desktop\Nowy folder\WEB-INF\classes\
 * Qualified Name:     core.RequestMessageDataTransfer
 * JD-Core Version:    0.7.0.1
 */