/*  1:   */ package core;
/*  2:   */ 
/*  3:   */ public class ResponseMessageDataTransfer
/*  4:   */ {
/*  5: 5 */   private String protocol = "";
/*  6: 6 */   private String session = "";
/*  7: 7 */   private String type = "";
/*  8: 8 */   private String status = "";
/*  9: 9 */   private String status_str = "";
/* 10:10 */   private Object data = "";
/* 11:11 */   private String respToSeq = "";
/* 12:12 */   private String respToType = "";
/* 13:   */   
/* 14:   */   public ResponseMessageDataTransfer(String protocol, String session, String type, String status, String status_str, Object data, String respToSeq, String respToType)
/* 15:   */   {
/* 16:20 */     this.protocol = protocol;
/* 17:21 */     this.session = session;
/* 18:22 */     this.type = type;
/* 19:23 */     this.status = status;
/* 20:24 */     this.status_str = status_str;
/* 21:25 */     this.data = data;
/* 22:26 */     this.respToSeq = respToSeq;
/* 23:27 */     this.respToType = respToType;
/* 24:   */   }
/* 25:   */   
/* 26:   */   public ResponseMessageDataTransfer() {}
/* 27:   */   
/* 28:   */   public String getProtocol()
/* 29:   */   {
/* 30:43 */     return this.protocol;
/* 31:   */   }
/* 32:   */   
/* 33:   */   public void setProtocol(String protocol)
/* 34:   */   {
/* 35:46 */     this.protocol = protocol;
/* 36:   */   }
/* 37:   */   
/* 38:   */   public String getSession()
/* 39:   */   {
/* 40:49 */     return this.session;
/* 41:   */   }
/* 42:   */   
/* 43:   */   public void setSession(String session)
/* 44:   */   {
/* 45:52 */     this.session = session;
/* 46:   */   }
/* 47:   */   
/* 48:   */   public String getType()
/* 49:   */   {
/* 50:55 */     return this.type;
/* 51:   */   }
/* 52:   */   
/* 53:   */   public void setType(String type)
/* 54:   */   {
/* 55:58 */     this.type = type;
/* 56:   */   }
/* 57:   */   
/* 58:   */   public String getStatus()
/* 59:   */   {
/* 60:61 */     return this.status;
/* 61:   */   }
/* 62:   */   
/* 63:   */   public void setStatus(String status)
/* 64:   */   {
/* 65:64 */     this.status = status;
/* 66:   */   }
/* 67:   */   
/* 68:   */   public String getStatus_str()
/* 69:   */   {
/* 70:67 */     return this.status_str;
/* 71:   */   }
/* 72:   */   
/* 73:   */   public void setStatus_str(String status_str)
/* 74:   */   {
/* 75:70 */     this.status_str = status_str;
/* 76:   */   }
/* 77:   */   
/* 78:   */   public Object getData()
/* 79:   */   {
/* 80:73 */     return this.data;
/* 81:   */   }
/* 82:   */   
/* 83:   */   public void setData(Object data)
/* 84:   */   {
/* 85:76 */     this.data = data;
/* 86:   */   }
/* 87:   */   
/* 88:   */   public String getRespToSeq()
/* 89:   */   {
/* 90:79 */     return this.respToSeq;
/* 91:   */   }
/* 92:   */   
/* 93:   */   public void setRespToSeq(String respToSeq)
/* 94:   */   {
/* 95:82 */     this.respToSeq = respToSeq;
/* 96:   */   }
/* 97:   */   
/* 98:   */   public String getRespToType()
/* 99:   */   {
/* :0:85 */     return this.respToType;
/* :1:   */   }
/* :2:   */   
/* :3:   */   public void setRespToType(String respToType)
/* :4:   */   {
/* :5:88 */     this.respToType = respToType;
/* :6:   */   }
/* :7:   */ }


/* Location:           C:\Users\T430\Desktop\Nowy folder\WEB-INF\classes\
 * Qualified Name:     core.ResponseMessageDataTransfer
 * JD-Core Version:    0.7.0.1
 */