/*   1:    */ package core;
/*   2:    */ 
/*   3:    */ import java.io.BufferedReader;
/*   4:    */ import java.io.IOException;
/*   5:    */ import java.io.InputStreamReader;
/*   6:    */ import java.io.OutputStream;
/*   7:    */ import java.io.PrintStream;
/*   8:    */ import java.net.HttpURLConnection;
/*   9:    */ import java.net.URL;
/*  10:    */ import javax.servlet.http.HttpSession;
/*  11:    */ import org.codehaus.jackson.JsonGenerationException;
/*  12:    */ import org.codehaus.jackson.map.JsonMappingException;
/*  13:    */ import org.codehaus.jackson.map.ObjectMapper;
/*  14:    */ 
/*  15:    */ public class NorthApiCommunicator
/*  16:    */ {
/*  17: 32 */   String address = null;
/*  18:    */   
/*  19:    */   public NorthApiCommunicator(HttpSession session)
/*  20:    */   {
/*  21: 35 */     System.out.println("session vbs addrr  :  " + (String)session.getAttribute("vbsAddress"));
/*  22: 36 */     this.address = ((String)session.getAttribute("vbsAddress"));
/*  23:    */   }
/*  24:    */   
/*  25:    */   public ResponseMessageDataTransfer sendMessage(RequestMessageDataTransfer dto)
/*  26:    */   {
/*  27: 51 */     ResponseMessageDataTransfer responseDto = null;
/*  28:    */     try
/*  29:    */     {
/*  30: 60 */       System.out.println("Trying to open connection to " + this.address);
/*  31:    */       
/*  32:    */ 
/*  33:    */ 
/*  34: 64 */       URL servConn = new URL("http://" + this.address);
/*  35:    */       
/*  36:    */ 
/*  37:    */ 
/*  38: 68 */       HttpURLConnection conn = (HttpURLConnection)servConn.openConnection();
/*  39:    */       
/*  40: 70 */       System.out.println("Connected to :   " + conn.getURL());
/*  41:    */       
/*  42: 72 */       conn.setDoOutput(true);
/*  43: 73 */       conn.setDoInput(true);
/*  44:    */       
/*  45:    */ 
/*  46:    */ 
/*  47:    */ 
/*  48:    */ 
/*  49: 79 */       conn.setRequestProperty("Content-Type", "application/json");
/*  50: 80 */       conn.setRequestProperty("Accept", "application/json");
/*  51: 81 */       conn.setRequestMethod("POST");
/*  52:    */       
/*  53:    */ 
/*  54:    */ 
/*  55:    */ 
/*  56:    */ 
/*  57:    */ 
/*  58:    */ 
/*  59:    */ 
/*  60:    */ 
/*  61:    */ 
/*  62:    */ 
/*  63: 93 */       OutputStream outputToNorthApi = conn.getOutputStream();
/*  64:    */       

/*  75:105 */       ObjectMapper mapper = new ObjectMapper();
/*  76:    */       
/*  77:    */ 
/*  78:108 */       mapper.writeValue(outputToNorthApi, dto);
/*  79:    */       try
/*  80:    */       {
/*  81:117 */         System.out.println(mapper.writeValueAsString(dto));
/*  82:    */       }
/*  83:    */       catch (JsonGenerationException e)
/*  84:    */       {
/*  85:121 */         e.printStackTrace();
/*  86:    */       }
/*  87:    */       catch (JsonMappingException e)
/*  88:    */       {
/*  89:125 */         e.printStackTrace();
/*  90:    */       }
/*  91:    */       catch (IOException e)
/*  92:    */       {
/*  93:129 */         e.printStackTrace();
/*  94:    */       }
/*  95:141 */       outputToNorthApi.flush();
/*  96:142 */       outputToNorthApi.close();
/*  97:    */       
/*  98:    */ 
/*  99:145 */       BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
/* 100:146 */       StringBuffer sb = new StringBuffer();
/* 101:147 */       String str = br.readLine();
/* 102:148 */       while (str != null)
/* 103:    */       {
/* 104:149 */         sb.append(str);
/* 105:150 */         str = br.readLine();
/* 106:    */       }
/* 107:153 */       br.close();
/* 108:    */       
/* 109:155 */       String responseString = sb.toString();
/* 110:    */       
/* 111:157 */       System.out.println(responseString);
/* 112:    */     }
/* 113:    */     catch (IOException e1)
/* 114:    */     {
/* 115:163 */       e1.printStackTrace();
/* 116:    */     }
/* 117:168 */     return responseDto;
/* 118:    */   }
/* 119:    */   
/* 120:    */   public ResponseMessageDataTransfer sendMessageUsingJson(String jsonString)
/* 121:    */   {
/* 122:177 */     ResponseMessageDataTransfer responseDto = null;
/* 123:    */     try
/* 124:    */     {
/* 125:184 */       System.out.println("Trying to open connection to " + this.address);
/* 126:185 */       URL servConn = new URL("http://" + this.address);
/* 127:    */       
/* 128:    */ 
/* 129:    */ 
/* 130:189 */       HttpURLConnection conn = (HttpURLConnection)servConn.openConnection();
/* 131:    */       
/* 132:191 */       System.out.println("Connected to :" + conn.getURL());
/* 133:    */       
/* 134:    */ 
/* 135:194 */       conn.setDoOutput(true);
/* 136:195 */       conn.setDoInput(true);

                    conn.setRequestProperty("Content-Type", "application/json");
/* 142:201 */       conn.setRequestProperty("Accept", "application/json");
/* 143:202 */       conn.setRequestMethod("POST");

/* 149:    */ 
/* 150:209 */       OutputStream outputToNorthApi = conn.getOutputStream();

/* 157:216 */       outputToNorthApi.write(jsonString.getBytes());
/* 158:    */       
/* 159:218 */       outputToNorthApi.flush();
/* 160:219 */       outputToNorthApi.close();
/* 161:    */       
/* 162:221 */       BufferedReader br = null;


/* 163:    */       try
/* 164:    */       {
/* 165:224 */         br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
/* 166:    */       }
/* 167:    */       catch (NullPointerException e )
/* 168:    */       {
/* 169:227 */         System.out.println("Server reponse is wrong.");
/* 170:    */       } catch (java.io.IOException ex) {
	
	                    
	                 System.out.println(" THE VBS IS DOWN.");      
	 
                    }


                       try{
                       StringBuffer sb = new StringBuffer();
                       String str = br.readLine();

                       while (str != null)

                       {
                       sb.append(str);
                       str = br.readLine();
                       }
                       br.close();

                          String responseString = sb.toString();
                          System.out.println(responseString);
                          
                          ObjectMapper mapper = new ObjectMapper();
                                 
                          responseDto = (ResponseMessageDataTransfer)mapper.readValue(responseString, ResponseMessageDataTransfer.class);

                      } catch (NullPointerException ex) {
                    	  
                    	  
                      }
      
      
     }
    catch (IOException e1)
    {
      e1.printStackTrace();
    }
    return responseDto;
   }
 }


