/*  1:   */ package core;
/*  2:   */ 
/*  3:   */ public class ReturnMultipleValues
/*  4:   */ {
/*  5:   */   public boolean passed;
/*  6:   */   public boolean issuper;
/*  7:   */   
/*  8:   */   public boolean isPassed()
/*  9:   */   {
/* 10:15 */     return this.passed;
/* 11:   */   }
/* 12:   */   
/* 13:   */   public void setPassed(boolean passed)
/* 14:   */   {
/* 15:19 */     this.passed = passed;
/* 16:   */   }
/* 17:   */   
/* 18:   */   public boolean isIssuper()
/* 19:   */   {
/* 20:23 */     return this.issuper;
/* 21:   */   }
/* 22:   */   
/* 23:   */   public void setIssuper(boolean issuper)
/* 24:   */   {
/* 25:27 */     this.issuper = issuper;
/* 26:   */   }
/* 27:   */ }


/* Location:           C:\Users\T430\Desktop\Nowy folder\WEB-INF\classes\
 * Qualified Name:     core.ReturnMultipleValues
 * JD-Core Version:    0.7.0.1
 */